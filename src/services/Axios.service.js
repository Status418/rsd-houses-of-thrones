import axios from 'axios';

const instance = axios.create({
	baseURL: 'https://anapioficeandfire.com/api/'
});

export default instance;
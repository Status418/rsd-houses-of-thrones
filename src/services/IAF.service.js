import AxiosInstace from './Axios.service';

export default {
	api: AxiosInstace,
	
	FetchData (url) {
		return new Promise(async (resolve, reject) => {
			let res = {
				data: [],
				message: 'Oops - somehow there are no houses. Please check back later',
				status: 404
			};
			try {
				const data = await this.api.get(url);
				if (data.status === 200) res.message = null;
				res.data = data.data;
				res.status = data.status;
				resolve(res);
			} catch (e) {
				console.log(e);
				res.message = 'Sorry i can`t hear the song of ice and Fire right now. Please check back later';
			}
			return reject(res);
		});
	},
	
	async HouseList () {
		return new Promise(async (resolve, reject) => {
			try {
				let list = [];
				let page = 1;
				let fetch = true;
				while (fetch) {
					let res = await this.FetchData(`/houses?page=${page}&pageSize=50`);
					if (res && res.data.length) list = [...list, ...res.data];
					else fetch = false;
					page++;
				}
				list = list.map((item) => this.HouseAddExtraData(item));
				resolve(list);
			} catch (e) {
				console.log(e);
				//res.message = 'Sorry i can`t hear the song of ice and Fire right now. Please check back later';
			}
			return reject();
		});
	},
	
	async HouseDetail (houseId) {
		return new Promise(async (resolve, reject) => {
			try {
				const res = await this.FetchData(`/houses/${houseId}`);
				if (res.status === 200) {
					res.data = this.HouseAddExtraData(res.data);
					res.message = null;
					resolve(res);
				}
			} catch (e) {
				reject();
				//res.message = 'Sorry i can`t hear the song of ice and Fire right now. Please check back later';
			}
			reject();
		})
	},
	
	HouseAddExtraData (house) {
		if (typeof house !== 'object' || !house.hasOwnProperty('url')) return false;
		house.id = Number(house.url.split('/').pop());
		house.link = `/Houses/${house.id}`;
		if (house.hasOwnProperty('overlord')) {
			const overlordId = Number(house.overlord.split('/').pop());
			house.overlordLink = `/Houses/${overlordId}`;
		}
		const chars = ['currentLord', 'heir', 'founder'];
		chars.forEach(async (el, index) => {
			if (house.hasOwnProperty(el)) {
				const id = Number(house[el].split('/').pop());
				if (id > 0) {
					try {
						const res = await this.CharaterDetail(id);
						if (res.status === 200) house[el] = res.data;
					} catch (e) {
					}
				}
			}
		});
		if (house.hasOwnProperty('swornMembers') && house.swornMembers.length) {
			
			house.swornMembers = house.swornMembers.map(async (item) => {
				const id = Number(item.split('/').pop());
				try {
					const res = await this.CharaterDetail(id);
					if (res.status === 200) house.swornMembers = res.data;
				} catch (e) {}
			});
		}
		if (house.hasOwnProperty('cadetBranches')) {
		
		}
		
		return house;
	},
	
	CharaterDetail (characterId) {
		return new Promise(async (resolve, reject) => {
			if (typeof characterId !== 'number') reject();
			try {
				const res = await this.FetchData(`/characters/${characterId}`);
				if (res.status === 200) {
					res.message = null;
				}
				resolve(res);
			} catch (e) {
				console.log(e);
				//res.message = 'Sorry i can`t hear the song of ice and Fire right now. Please check back later';
			}
		})
	}
}
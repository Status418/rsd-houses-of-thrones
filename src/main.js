import Vue from 'vue';
import VueCompositionApi from '@vue/composition-api';
import vuescroll from 'vuescroll';
import App from './App.vue';
import router from './router';
import store from './store';

Vue.use(VueCompositionApi);
Vue.use(vuescroll);
Vue.config.productionTip = false;

new Vue({
	router,
	store,
	render: h => h(App)
}).$mount('#app');
import AIFService from '../../services/IAF.service';
import store from '../index';

export const namespaced = true;

export const state = {
	houseList: [],
	loading: false
};

export const mutations = {
	SET_LIST (state, list) {
		state.houseList = list;
	},
	MERGE_HOUSE (state, house) {
		if (typeof state.houseList.find(el => el.id == house.id) !== 'object') {
			state.houseList = [...state.houseList, ...[house]]
		}
	},
	SET_LOADING (state, isLoading) {
		state.loading = isLoading;
	}
};

export const actions = {
	async housListGet ({commit, state}) {
		try {
			commit('SET_LOADING', true);
			const data = await AIFService.HouseList();
			if (data.length) {
				commit('SET_LIST', data);
			}
		} catch (e) {
			console.log(e);
			//TODO: notify or redirect the user about the failure
		}
		commit('SET_LOADING', false);
	},
	
	async houseDetailGet ({commit, state}, id) {
		if (!id) return;
		let data = state.houseList.find(house => house.id == id.id);
		if (typeof data === 'object') return;
		try {
			commit('SET_LOADING', true);
			const data = await AIFService.HouseDetail(id.id);
			if (typeof data === 'object') {
				commit('MERGE_HOUSE', data.data);
			}
		} catch (e) {
			console.log('store/AIF.js - houseDetailGet()', e);
			//TODO: notify or redirect the user about the failure
		}
		commit('SET_LOADING', false);
	}
};

export const getters = {
	houseDetail: state => x => {
		const house = state.houseList.find(house => house.id == x);
		return house ? house : {};
	}
};
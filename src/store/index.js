import Vue from 'vue';
import Vuex from 'vuex';

import * as AIF from './modules/AIF.js';

Vue.use(Vuex);

export default new Vuex.Store({
	modules: {
		AIF
	},
	state: {
	
	},
});